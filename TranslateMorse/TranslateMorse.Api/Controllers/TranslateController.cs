﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using TranslateMorse.Api.Controllers;
using TranslateMorse.Api.DTOs;
using TranslateMorse.Application.Services.Contracts;

namespace ChallengeMorse.Controllers
{
    [Route("api/translate")]
    [ApiController]
    public class TranslateController : BaseController
    {
        #region Properties
        private readonly ILogger<TranslateController> _logger;
        private readonly ITextService _textService;
        #endregion

        #region Constructor
        public TranslateController(ILogger<TranslateController> logger, ITextService textService)
            : base("Translate", logger)
        {
            _logger = logger;
            _textService = textService;
        }
        #endregion


        // POST: api/translate/2-human
        [HttpPost("2-text")]
        [SwaggerOperation(
            Summary =
                "Dado un string en MORSE y retorna un string legible por un humano.",
            Description = "",
            OperationId = "Translate2Human"
        )]
        [SwaggerResponse(200, "Se obtuvo traducción.", typeof(string))]
        [SwaggerResponse(404, "No se obtuvo traducción.", typeof(string))]
        public ActionResult<string> Translate2Human([FromBody] Translate2HumanRequestDto morseCode)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            string result = _textService.Translate2Human(morseCode.Text);
            return Ok(result);           
        }


        // POST: api/translate/2-morse
        [HttpPost("2-morse")]
        [SwaggerOperation(
            Summary =
                "Dado un string legible por un humano y retorna un string en código MORSE.",
            Description = "",
            OperationId = "Translate2Morse"
        )]
        [SwaggerResponse(200, "Se obtuvo traducción.", typeof(string))]
        [SwaggerResponse(404, "No se obtuvo traducción.", typeof(string))]
        public ActionResult<string> Translate2Morse([FromBody] Translate2MorseRequestDto textHuman)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            string result = _textService.Translate2Morse(textHuman.Text);
            return Ok(result);
        }
    }
}