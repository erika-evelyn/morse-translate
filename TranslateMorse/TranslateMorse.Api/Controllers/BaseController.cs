﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TranslateMorse.Api.Controllers
{
    [Route("api/[controller]")]
    public class BaseController : Controller
    {
        protected BaseController(string nombreApi, ILogger logger)
        {
            logger.LogInformation("Inicio de " + nombreApi);
        }
    }
}
