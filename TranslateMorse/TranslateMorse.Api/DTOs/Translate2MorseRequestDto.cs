﻿using System.ComponentModel.DataAnnotations;

namespace TranslateMorse.Api.DTOs
{
    public class Translate2MorseRequestDto
    {
        [Required(ErrorMessage = "{0} es obligatorio.")]
        [RegularExpression(@"^([0-9a-zA-Z\s]+)$", ErrorMessage = "Solo se adminten letras (sin tilde), números y espacios.")]
        public string Text { get; set; }
    }
}
