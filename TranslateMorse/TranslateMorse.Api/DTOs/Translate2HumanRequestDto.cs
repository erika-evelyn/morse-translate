﻿using System.ComponentModel.DataAnnotations;

namespace TranslateMorse.Api.DTOs
{
    public class Translate2HumanRequestDto
    {
        [Required(ErrorMessage = "{0} es obligatorio.")]
        [RegularExpression(@"^([\.\-\s]+)$", ErrorMessage = "Código MORSE inválido.")]
        public string Text { get; set; }
    }
}
