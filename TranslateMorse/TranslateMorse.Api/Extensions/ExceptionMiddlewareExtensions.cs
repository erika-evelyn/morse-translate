﻿using Microsoft.AspNetCore.Builder;
using TranslateMorse.Api.CustomExceptionMiddleware;

namespace TranslateMorse.Api.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
