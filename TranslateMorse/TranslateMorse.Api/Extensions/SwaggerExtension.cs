﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;

namespace TranslateMorse.Api.Extensions
{
    public static class SwaggerExtension
    {
        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var groupName = "v1";

                options.SwaggerDoc(groupName, new OpenApiInfo
                {
                    Title = $"Translate Morse {groupName}",
                    Version = groupName,
                    Description = "Translate to Morse API",
                    Contact = new OpenApiContact
                    {
                        Name = "Erika Rodriguez",
                        Email = "erikaevelynrodriguez@gmail.com",
                        Url = new Uri("https://www.linkedin.com/in/erika-evelyn-rodriguez-1320/"),
                    }
                });
            });
        }
    }
}
