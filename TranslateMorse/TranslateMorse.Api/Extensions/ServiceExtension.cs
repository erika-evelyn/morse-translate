﻿using Microsoft.Extensions.DependencyInjection;
using TranslateMorse.Application.Services.Contracts;
using TranslateMorse.Application.Services.Implementations;

namespace TranslateMorse.Api.Extensions
{
    public static class ServiceExtension
    {
        public static void SetDependencyInjection(this IServiceCollection services)
        {
            services.AddScoped<IBitsService, BitsService>();
            services.AddScoped<ITextService, TextService>();
        }
    }
}
