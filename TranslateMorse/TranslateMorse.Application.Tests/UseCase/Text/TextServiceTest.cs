﻿using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TranslateMorse.Application.Constants;
using TranslateMorse.Application.Services.Implementations;
using TranslateMorse.Support.Helpers;

namespace TranslateMorse.Application.Tests.UseCase.Text
{
    [TestClass]
    public class TextServiceTest
    {
        [TestMethod]
        public void Translate2Human_Get_translation_OK()
        {
            //Arrange
            var logger = new LoggerFactory().CreateLogger<TextService>();
            var sut = new TextService(logger);

            //Act
            var result = sut.Translate2Human(".... --- .-.. .-   -- . .-.. .. .-.-.-");

            //Assert
            var expected = "HOLA MELI.";
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        [ExpectedException(typeof(TraslateException), ErrorConstants.Translate2Human)]
        public void Translate2Human_GivenInvalidMorseCode_WhenCalled_ThenThrowTraslateException()
        {
            //Arrange
            var logger = new LoggerFactory().CreateLogger<TextService>();
            var sut = new TextService(logger);

            //Act
            var result = sut.Translate2Human("....---.-...-    --..-....");
        }


        [TestMethod]
        public void Translate2Morse_Get_translation_OK()
        {
            //Arrange
            var logger = new LoggerFactory().CreateLogger<TextService>();
            var sut = new TextService(logger);

            //Act
            var result = sut.Translate2Morse("HOLA MELI");

            //Assert
            var expected = ".... --- .-.. .-   -- . .-.. .. .-.-.-";
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        [ExpectedException(typeof(TraslateException), ErrorConstants.Translate2Morse)]
        public void Translate2Morse_GivenInvalidCharacter_WhenCalled_ThenThrowTraslateException()
        {
            //Arrange
            var logger = new LoggerFactory().CreateLogger<TextService>();
            var sut = new TextService(logger);

            //Act
            var result = sut.Translate2Morse("?");
        }

        [TestMethod]
        public void VerifySignalsMorseCodeAreValid_GivenValidMorseCode_WhenCalled_ThenReturnTrue()
        {
            //Arrange
            var logger = new LoggerFactory().CreateLogger<TextService>();
            var sut = new TextService(logger);

            //Act
            var result = sut.VerifySignalsMorseCodeAreValid(".... --- .-.. .-    -- . .-.. ..");

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void VerifySignalsMorseCodeAreValid_GivenInalidMorseCode_WhenCalled_ThenReturnFalse()
        {
            //Arrange
            var logger = new LoggerFactory().CreateLogger<TextService>();
            var sut = new TextService(logger);

            //Act
            var result = sut.VerifySignalsMorseCodeAreValid("....---.-..");

            //Assert
            Assert.IsFalse(result);
        }
    }
}
