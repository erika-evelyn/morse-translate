﻿using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections;
using System.Linq;
using TranslateMorse.Application.Constants;
using TranslateMorse.Application.Services.Contracts;
using TranslateMorse.Application.Services.Implementations;
using TranslateMorse.Support.Helpers;

namespace TranslateMorse.Application.Tests.UseCase.Bits
{
    [TestClass]
    public class BitsServiceTest
    {
        [TestMethod]
        public void DecodeBits2Morse_GivenValidBitsSequence_WhenCalled_ThenReturnCorrectMorseCode()
        {
            //Arrange
            var logger = new LoggerFactory().CreateLogger<BitsService>();
            MockObject mock = new MockObject();

            mock.TextService.Setup(x => x.VerifySignalsMorseCodeAreValid(It.IsAny<string>()))
                    .Returns(true);

            var sut = new BitsService(logger, mock.TextService.Object);

            string input = "000000000011011011001110000011111100011111100111111000000011101111111101110111000000011000111111000000000001111110011111100000001100000011011111111011101110000001101110000001100111111001100111111001100111111";
            var bits = new BitArray(input.Select(c => c == '1').ToArray());

            //Act
            var result = sut.DecodeBits2Morse(bits);

            //Assert
            var expected = ".... --- .-.. .-    -- . .-.. .. .-.-.-";
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        [ExpectedException(typeof(TraslateException), ErrorConstants.DecodeBits2Morse)]
        public void DecodeBits2Morse_GivenInvalidBitsSequenceToMorseCode_WhenCalled_ThenThrowTraslateException()
        {
            //Arrange
            var logger = new LoggerFactory().CreateLogger<BitsService>();
            MockObject mock = new MockObject();

            mock.TextService.Setup(x => x.VerifySignalsMorseCodeAreValid(It.IsAny<string>()))
                    .Returns(false);

            var sut = new BitsService(logger, mock.TextService.Object);

            string input = "1101101101101111110011011011011";
            var bits = new BitArray(input.Select(c => c == '1').ToArray());

            //Act
            var result = sut.DecodeBits2Morse(bits);
        }


        [TestMethod]
        public void DecodeBits2Morse_GivenBitSequenceAtZero_WhenCalled_ThenReturnStringEmpy()
        {
            //Arrange
            var logger = new LoggerFactory().CreateLogger<BitsService>();
            MockObject mock = new MockObject();

            mock.TextService.Setup(x => x.VerifySignalsMorseCodeAreValid(It.IsAny<string>()))
                    .Returns(true);

            var sut = new BitsService(logger, mock.TextService.Object);

            string input = "00000000000000000000";
            var bits = new BitArray(input.Select(c => c == '1').ToArray());

            //Act
            var result = sut.DecodeBits2Morse(bits);

            //Assert
            var expected = string.Empty;
            Assert.AreEqual(expected, result);
        }


        class MockObject
        {

            public MockObject()
            {
                TextService = new Mock<ITextService>();
            }

            public Mock<ITextService> TextService { get; set; }

        }
    }


}
