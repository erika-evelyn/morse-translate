﻿using System;

namespace TranslateMorse.Support.Helpers
{
    public class TraslateException : Exception
    {
        public TraslateException(string message) : base(message) { }

        public TraslateException(string message, Exception inner) : base(message, inner) { }

    }
}
