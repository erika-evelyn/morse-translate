﻿using System;

namespace TranslateMorse.Support.Helpers
{
    public static class ExceptionExtensions
    {
        public static string GetExceptionMessages(this Exception e, string msgs = "")
        {
            if (e == null) return string.Empty;

            if (e.InnerException != null)
            {
                msgs += GetExceptionMessages(e.InnerException);
            }
            else
            {
                msgs = e.Message;
            }

            return msgs;
        }
    }
}
