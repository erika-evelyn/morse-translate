﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TranslateMorse.Api.Tests.Helpers;
using TranslateMorse.Application.Services.Contracts;

namespace TranslateMorse.Api.Tests
{
    [TestClass]
    public class DependencyResolverTest
    {
        private DependencyResolverHelper _serviceProvider;

        public DependencyResolverTest()
        {

            var webHost = WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .Build();
            _serviceProvider = new DependencyResolverHelper(webHost);
        }

        [TestMethod]
        public void Service_Should_Get_Resolved()
        {

            //Act
            var yourService = _serviceProvider.GetService<IBitsService>();

            //Assert
            Assert.IsNotNull(yourService);
        }
    }
}
