﻿using ChallengeMorse.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TranslateMorse.Api.DTOs;
using TranslateMorse.Application.Services.Contracts;

namespace TranslateMorse.Api.Tests.Controllers
{
    [TestClass]
    public class TranslateControllerTest
    {

        [TestMethod]
        public void Translate2Human_GivenValidRequest_WhenCalled_ThenReturnSuccess()
        {
            MockObject mock = new MockObject();

            mock.TextService.Setup(x => x.Translate2Human(It.IsAny<string>()))
                    .Returns("A");
            var logger = new LoggerFactory().CreateLogger<TranslateController>();

            var sut = new TranslateController(logger, mock.TextService.Object);

            var result = sut.Translate2Human(new Translate2HumanRequestDto { Text = ".-" });

            result.Should().NotBeNull();
            Assert.IsInstanceOfType(result, typeof(ActionResult<string>));
            Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));                    
            Assert.AreEqual("A", ((OkObjectResult)result.Result).Value);
        }


        [TestMethod]
        public void Translate2Morse_GivenValidRequest_WhenCalled_ThenReturnSuccess()
        {
            MockObject mock = new MockObject();

            mock.TextService.Setup(x => x.Translate2Morse(It.IsAny<string>()))
                    .Returns(".-");
            var logger = new LoggerFactory().CreateLogger<TranslateController>();

            var sut = new TranslateController(logger, mock.TextService.Object);

            var result = sut.Translate2Morse(new Translate2MorseRequestDto { Text = "A" });

            result.Should().NotBeNull();
            Assert.IsInstanceOfType(result, typeof(ActionResult<string>));
            Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
            Assert.AreEqual(".-", ((OkObjectResult)result.Result).Value);
        }


        [TestMethod]
        public void Translate2Human_GivenInvalidRequest_WhenCalled_ThenReturnBadRequest()
        {
            MockObject mock = new MockObject();

            mock.TextService.Setup(x => x.Translate2Human(It.IsAny<string>()))
                    .Returns("A");
            var logger = new LoggerFactory().CreateLogger<TranslateController>();

            var sut = new TranslateController(logger, mock.TextService.Object);
            sut.ModelState.AddModelError("fakeError", "fakeError");

            var result = sut.Translate2Human(new Translate2HumanRequestDto { Text = ".-" });

            result.Should().NotBeNull();
            Assert.IsInstanceOfType(result.Result, typeof(BadRequestObjectResult));
        }


        [TestMethod]
        public void Translate2Morse_GivenInvalidRequest_WhenCalled_ThenReturnBadRequest()
        {
            MockObject mock = new MockObject();

            mock.TextService.Setup(x => x.Translate2Morse(It.IsAny<string>()))
                    .Returns(".-");
            var logger = new LoggerFactory().CreateLogger<TranslateController>();

            var sut = new TranslateController(logger, mock.TextService.Object);
            sut.ModelState.AddModelError("fakeError", "fakeError");

            var result = sut.Translate2Morse(new Translate2MorseRequestDto { Text = "A" });

            result.Should().NotBeNull();
            Assert.IsInstanceOfType(result.Result, typeof(BadRequestObjectResult));
        }


        class MockObject {

            public MockObject()
            {
                TextService = new Mock<ITextService>();
            }

            public Mock<ITextService> TextService { get; set; }

        }
    }
}
