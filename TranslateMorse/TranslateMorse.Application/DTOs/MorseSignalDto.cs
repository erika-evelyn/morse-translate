﻿namespace TranslateMorse.Application.DTOs
{
    public class MorseSignalDto
    {
        public int Lenght { get; set; }
        public bool IsPulse { get; set; }
        public string Representation { get; set; }
    }
}
