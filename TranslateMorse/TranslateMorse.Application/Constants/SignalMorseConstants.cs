﻿namespace TranslateMorse.Application.Constants
{
    public static class SignalMorseConstants
    {
        public const string LettersSeparation = " ";
        public const string WordsSeparation = "    ";
        public const string Stroke = "-";
        public const string Point = ".";
        public const string Fullstop = ".-.-.-";
    }
}
