﻿namespace TranslateMorse.Application.Constants
{
    public class ErrorConstants
    {
        public const string DecodeBits2Morse = "Ha ocurrido un error al traducir la secuencia de bits a MORSE.";
        public const string Translate2Human = "Ha ocurrido un error al traducir el código MORSE.";
        public const string Translate2Morse = "Ha ocurrido un error al traducir a código MORSE.";
    }
}
