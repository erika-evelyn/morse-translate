﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using TranslateMorse.Application.Constants;
using TranslateMorse.Application.Services.Contracts;
using TranslateMorse.Support.Helpers;

namespace TranslateMorse.Application.Services.Implementations
{
    public class TextService : ITextService
    {
        #region Properties
        private readonly ILogger<TextService> _logger;
        #endregion

        #region Constructor
        public TextService(ILogger<TextService> logger)
        {
            _logger = logger;
        }
        #endregion

        #region Public Methods
        public string Translate2Human(string morseCode)
        {
            _logger.LogDebug("Inicia Translate2Human");
            Dictionary<string, char> morseToAlphabetDictionary = CreateMorseToAlphabetDictionary();
            string text = string.Empty;
            string wordsSeparation = "  "; // Solo 2 espacios porque ya se agrega uno del caracter anterior (terminan siendo 3).
            try
            {
                if (!VerifySignalsMorseCodeAreValid(morseCode))
                    throw new TraslateException("El código morse ingresado es inválido.");

                string[] words = morseCode.Split(wordsSeparation);

                foreach (var word in words)
                {
                    string[] letters = word.Split(SignalMorseConstants.LettersSeparation);

                    foreach (var letter in letters)
                    {
                        if (!string.IsNullOrEmpty(letter))
                            text += morseToAlphabetDictionary[letter];
                    }
                    text += " ";
                }
            }
            catch (TraslateException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.GetExceptionMessages());

                throw new TraslateException(ErrorConstants.Translate2Human);
            }

            return text.Trim();
        }

        public string Translate2Morse(string textHuman)
        {
            _logger.LogDebug("Inicia Translate2Morse");
            Dictionary<char, string> alphabetToMorseDictionary = CreateAlphabetToMorseDictionary();
            string morseCode = string.Empty;
            string wordSeparation = "  "; // Solo 2 espacios porque ya se agrega uno del caracter anterior (terminan siendo 3).
            try
            {
                foreach (var letter in textHuman.ToUpper())
                {
                    if (letter == ' ')
                        morseCode += wordSeparation;
                    else
                        morseCode += alphabetToMorseDictionary[letter] + SignalMorseConstants.LettersSeparation;
                }
                morseCode += SignalMorseConstants.Fullstop;

                if (!VerifySignalsMorseCodeAreValid(morseCode))
                    throw new TraslateException(ErrorConstants.Translate2Morse);
            }
            catch (TraslateException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.GetExceptionMessages());

                throw new TraslateException(ErrorConstants.Translate2Morse);
            }
            return morseCode;
        }


        /// <summary>
        /// Recibe por parametro una secuencia de codigo morse y verifica que todas las señales incluídas en él, correspondan
        /// a señales válidas de código MORSE. De ser así retorna true, de lo contrario false (con log de error).
        /// </summary>
        /// <param name="morseCode"></param>
        /// <returns></returns>
        public bool VerifySignalsMorseCodeAreValid(string morseCode)
        {
            var signals = morseCode.Split(SignalMorseConstants.LettersSeparation);
            Dictionary<string, char> morseToAlphabetDictionary = CreateMorseToAlphabetDictionary();

            foreach (var signal in signals)
            {
                if (!string.IsNullOrEmpty(signal))
                {
                    if (!morseToAlphabetDictionary.TryGetValue(signal, out _))
                    {
                        _logger.LogError("Durante la traducción, se generó el código [{0}] que no corresponde al alfabeto MORSE.", signal);
                        return false;
                    }

                }
            }
            return true;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Retorna un diccionario con clave codigo MORSE y como valor el caracter legible por humano (alfabeto) que lo representa.
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, char> CreateMorseToAlphabetDictionary()
        {
            Dictionary<string, char> morseToAlphabetDictionary = new Dictionary<string, char>()
            {
                {".-", 'A'},
                {"-...", 'B'},
                { "-.-.", 'C'},
                {"-..", 'D'},
                {".", 'E'},
                {"..-.", 'F'},
                {"--.", 'G'},
                {"....", 'H'},
                {"..", 'I'},
                {".---", 'J'},
                {"-.-", 'K'},
                {".-..", 'L'},
                {"--", 'M'},
                {"-.", 'N'},
                {"---", 'O'},
                {".--.", 'P'},
                {"--.-", 'Q'},
                {".-.", 'R'},
                {"...", 'S'},
                {"-", 'T'},
                {"..-", 'U'},
                {"...-", 'V'},
                {".--", 'W'},
                {"-..-", 'X'},
                {"-.--", 'Y'},
                {"--..", 'Z'},
                {"-----", '0'},
                {".----", '1'},
                {"..---", '2'},
                {"...--", '3'},
                {"....-", '4'},
                {".....", '5'},
                {"-....", '6'},
                {"--...", '7'},
                {"---..", '8'},
                {"----.", '9'},
                {".-.-.-", '.'},
            };

            return morseToAlphabetDictionary;
        }

        /// <summary>
        /// Retorna un diccionario con clave caracter legible por humano (alfabeto) y como valor el codigo MORSE que lo representa.
        /// </summary>
        /// <returns></returns>
        private Dictionary<char, string> CreateAlphabetToMorseDictionary()
        {
            Dictionary<char, string> alphabetToMorseDictionary = new Dictionary<char, string>()
            {
                {'A' , ".-"},
                {'B' , "-..."},
                {'C' , "-.-."},
                {'D' , "-.."},
                {'E' , "."},
                {'F' , "..-."},
                {'G' , "--."},
                {'H' , "...."},
                {'I' , ".."},
                {'J' , ".---"},
                {'K' , "-.-"},
                {'L' , ".-.."},
                {'M' , "--"},
                {'N' , "-."},
                {'O' , "---"},
                {'P' , ".--."},
                {'Q' , "--.-"},
                {'R' , ".-."},
                {'S' , "..."},
                {'T' , "-"},
                {'U' , "..-"},
                {'V' , "...-"},
                {'W' , ".--"},
                {'X' , "-..-"},
                {'Y' , "-.--"},
                {'Z' , "--.."},
                {'0' , "-----"},
                {'1' , ".----"},
                {'2' , "..---"},
                {'3' , "...--"},
                {'4' , "....-"},
                {'5' , "....."},
                {'6' , "-...."},
                {'7' , "--..."},
                {'8' , "---.."},
                {'9' , "----."}
            };

            return alphabetToMorseDictionary;
        }



        #endregion
    }
}
