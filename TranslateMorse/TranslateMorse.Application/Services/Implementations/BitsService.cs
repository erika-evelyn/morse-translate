﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using TranslateMorse.Application.Constants;
using TranslateMorse.Application.DTOs;
using TranslateMorse.Application.Services.Contracts;
using TranslateMorse.Support.Helpers;

namespace TranslateMorse.Application.Services.Implementations
{
    public class BitsService : IBitsService
    {
        #region Properties
        private readonly ILogger<BitsService> _logger;
        private readonly ITextService _textService;
        #endregion

        #region Constructor
        public BitsService(ILogger<BitsService> logger, ITextService textService)
        {
            _logger = logger;
            _textService = textService;
        }
        #endregion

        #region Public Methods
        public string DecodeBits2Morse(BitArray bits)
        {
            try
            {
                _logger.LogDebug("Inicia DecodeBits2Morse");

                var result = ConvertBitSequenceToMorseSignals(bits);

                if (result.minLenghtPulse == 0)
                    return string.Empty;

                var morseCode = GetRepresentationSignalsToMorse(result.signals, result.minLenghtPulse);

                if (!_textService.VerifySignalsMorseCodeAreValid(morseCode))
                    throw new TraslateException(ErrorConstants.DecodeBits2Morse);

                return morseCode;
            }
            catch (TraslateException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.GetExceptionMessages());                
               
                throw new TraslateException(ErrorConstants.DecodeBits2Morse);
            }           
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Recorre la secuencia de bits y va separandolos en pausas y pulsos.
        /// </summary>
        /// <param name="bits"></param>
        /// <returns></returns>
        private (int minLenghtPulse, List<MorseSignalDto> signals) ConvertBitSequenceToMorseSignals(BitArray bits)
        {
            var minLenghtPulse = 0;
            var lastIsPulse = bits[0];
            var currentBits = new List<bool>();
            var signals = new List<MorseSignalDto>();

            for (var i = 0; i < bits.Count; i++)
            {
                bool bit = (bool)bits[i];

                if (lastIsPulse != bit)
                {
                    if (lastIsPulse && (minLenghtPulse == 0 || currentBits.Count < minLenghtPulse))
                        minLenghtPulse = currentBits.Count;

                    signals.Add(new MorseSignalDto
                    {
                        IsPulse = lastIsPulse,
                        Lenght = currentBits.Count
                    });
                    currentBits = new List<bool>();
                    lastIsPulse = !lastIsPulse;
                }
                currentBits.Add(bit);
                if (i == bits.Count - 1)
                {
                    signals.Add(new MorseSignalDto
                    {
                        IsPulse = lastIsPulse,
                        Lenght = currentBits.Count
                    });
                }
            }
            return (minLenghtPulse, signals);
        }
        /// <summary>
        /// Recibe por parametro las señales morse (pausas y pulsos) y la longitud minima de un pulso. A partir de este último,
        /// se calcula la longitud de las pausas que separan letras, pausas que separan palabras, los pulsos y los trazos.
        /// </summary>
        /// <param name="signals"></param>
        /// <param name="minLenghtPulse"></param>
        /// <returns></returns>
        private string GetRepresentationSignalsToMorse(List<MorseSignalDto> signals, int minLenghtPulse)
        {
            // una pausa equivale a un punto.
            // separación de letras equivale a una raya o tres puntos.  
            // separación de palabras equivale a seis puntos.
            var minStroke = minLenghtPulse * 3;
            var minPauseSplitLetters = minStroke - 1; //menos uno para aceptar variacion
            var minPauseSplitWords = (minPauseSplitLetters * 2) - 1; //menos uno para aceptar variacion
            var result = string.Empty;
            foreach (var signal in signals)
            {
                signal.Representation = GetRepresentationSignalToMorse(signal, minPauseSplitLetters, minPauseSplitWords, minStroke);

                result += signal.Representation;
            }
            return result.Trim();
        }

        /// <summary>
        /// Recibe por parametro una señal morse, la longitud minima de las pausas para separar letras y palabras,
        /// y la longitud minima de un trazo. A partir de estas longitudes, determina la representación de la señal recibida por parámetro 
        /// (pausa que separa letra, pausa que separa palabras, pulso o trazo) y la retorna.
        /// </summary>
        /// <param name="signal"></param>
        /// <param name="minPauseSplitLetters"></param>
        /// <param name="minPauseSplitWords"></param>
        /// <param name="minStroke"></param>
        /// <returns></returns>
        private string GetRepresentationSignalToMorse(MorseSignalDto signal, int minPauseSplitLetters,
            int minPauseSplitWords, int minStroke)
        {
            if (signal.IsPulse)
            {
                if (signal.Lenght < minStroke)
                    return SignalMorseConstants.Point;

                return SignalMorseConstants.Stroke;
            }
            else
            {
                if (signal.Lenght >= minPauseSplitWords)
                    return SignalMorseConstants.WordsSeparation;

                if (signal.Lenght >= minPauseSplitLetters)
                    return SignalMorseConstants.LettersSeparation;

                return string.Empty;
            }
        }


        #endregion
    }
}
