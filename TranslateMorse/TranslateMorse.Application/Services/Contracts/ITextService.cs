﻿namespace TranslateMorse.Application.Services.Contracts
{
    public interface ITextService
    {
        string Translate2Human(string morseCode);
        string Translate2Morse(string textHuman);
        bool VerifySignalsMorseCodeAreValid(string morseCode);
    }
}
