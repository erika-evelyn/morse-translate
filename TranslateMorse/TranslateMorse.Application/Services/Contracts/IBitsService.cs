﻿using System.Collections;

namespace TranslateMorse.Application.Services.Contracts
{
    public interface IBitsService
    {
        string DecodeBits2Morse(BitArray bits);
    }
}
