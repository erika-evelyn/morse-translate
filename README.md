# Translate Morse

El proyecto contiene distintas funcionalidades que permiten traducir de/a código MORSE:
 - Dado un array de bits, retorna un string con el resultado en MORSE.
 - Dado un string en MORSE, retorna un string legible por un humano. 
 - Dado un string legible por un humano, retorna un string en MORSE.


## API Rest 🚀

Expone 2 endpoints:

 1.  Traductor de texto a MORSE: 
 ```
 curl -X POST "https://translatemorseapi.azurewebsites.net/api/translate/2-morse" -H  "accept: text/plain" -H  "Content-Type: application/json" -d "{\"text\":\"HOLA MELI\"}"
 ```
 
 2. Traductor de MORSE a texto: 
 ```
 curl -X POST "https://translatemorseapi.azurewebsites.net/api/translate/2-text" -H  "accept: text/plain" -H  "Content-Type: application/json" -d "{\"text\":\".... --- .-.. .-   -- . .-.. .. .-.-.-\"}"
 ```
 
 [Ver documentación en Swagger](https://translatemorseapi.azurewebsites.net/swagger/index.html)


## Ejecución de pruebas ⚙️

Pruebas unitarias utilizando [MSTest](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-mstest)


## Despliegue 📦

Publicado con [Azure App Service](https://azure.microsoft.com/es-es/services/app-service/)


## Construido con 🛠️

[C#](https://docs.microsoft.com/es-es/dotnet/csharp/getting-started/)

[.NET Core 3.1](https://docs.microsoft.com/en-us/dotnet/fundamentals/)


## Referencias 

[Documentación MORSE](http://www.asifunciona.com/tablas/codigo_morse/codigo_morse_1.htm)



⌨️ por [Erika Rodriguez](https://www.linkedin.com/in/erika-evelyn-rodriguez-1320/) 😊